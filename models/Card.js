export class Card {
    constructor(title, dailyCur, dailyPre, weeklyCur, weeklyPre, monthlyCur, monthlyPre) {
        this.title = title;
        this.timeframes = {
            daily : {
                current: dailyCur,
                previous: dailyPre,
                name: "Yesterday"
            },
            weekly : {
                current: weeklyCur,
                previous: weeklyPre,
                name: "Last Week"
            },
            monthly : {
                current: monthlyCur,
                previous: monthlyPre,
                name: "Last Month"
            }
        }
    }
}