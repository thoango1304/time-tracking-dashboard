export class CardList {
    constructor() {
        this.list = [];
    }

    addCard(card) {
        this.list.push(card);
    }

    renderCardList(timeName) {
        let content = "";
        let copyOfThis = this;

        content = this.list.reduce(function (cardContent, card) {
            let obj = copyOfThis.getTime(timeName, card);
            
            cardContent += copyOfThis.renderHTML(card, obj);

            return cardContent;
        }, "")

        return content;
    }

    renderHTML(card, obj) {
        return `
            <div class="col-12 col-md-6 col-lg-4">
                <div class="timeCard">
                    <div class="${card.title.toLowerCase().replace(" ", "-")}">
                            
                    </div>
                    <div class="timeCard__info">
                        <div class="d-flex justify-content-between">
                            <span class="timeCard__title">${card.title}</span>
                            <svg width="21" height="5" xmlns="http://www.w3.org/2000/svg"><path d="M2.5 0a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5Zm8 0a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5Zm8 0a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5Z" fill="#BBC0FF" fill-rule="evenodd"/></svg>
                        </div>
                        <div class="d-flex justify-content-between d-lg-block">
                            <h2>
                                <span class="timeCard__current">${obj.current}</span>hrs
                            </h2>
                            <p class="text-right text-lg-left d-block">
                                <span class="timeCard__time">${obj.name}</span> -<br class="d-block d-lg-none"> <span class="timeCard__previous">${obj.previous}</span>hrs
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }

    getTime(timeName, card) {
        switch (timeName) {
            case "daily":
                return card.timeframes.daily;
            case "weekly":
                return card.timeframes.weekly;
            case "monthly":
                return card.timeframes.monthly;
        }
    }
}