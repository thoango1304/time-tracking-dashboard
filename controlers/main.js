import { Card } from "../models/Card.js";
import { CardList } from "../models/CardList.js";

let cardList = new CardList();

const renderHtml = () => {
    fetch('../data/data.json')
        .then((response) => response.json())
        .then((data) => {
            for (let obj of data) {
                let daily = obj.timeframes.daily;
                let weekly = obj.timeframes.weekly;
                let monthly = obj.timeframes.monthly;

                let newCard = new Card(obj.title, daily.current, daily.previous, weekly.current, weekly.previous, monthly.current, monthly.previous);
                cardList.addCard(newCard);
                
                renderCardList("weekly");
            }
        })
}

renderHtml();

const renderCardList = (timeName) => {
    let dashboardContent = document.getElementById("dashboard__content");
    
    dashboardContent.innerHTML = cardList.renderCardList(timeName);
}

const setTimely = () => {
    let timely = document.getElementsByClassName("timely");

    for (let item of timely) {
        item.addEventListener("click", () => {
            let name = addActiveClass(item, "timely");
            
            renderCardList(name);
        });
    }
}

setTimely();

const addActiveClass = (item, className) => {
    if (!item.classList.contains("active")) {
        document.querySelector(`.${className}.active`).classList.remove("active");
        item.classList.add("active");
    }
    return item.innerText.toLowerCase();
}